﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProyectoAlumno.Models
{
    [Serializable]
    public class Alumno
    {
        [Key]
        public int id { get; set; }
        [Required]
        [MaxLength(30)]
        [Display(Name ="Nombre")]
         public string nombre { get; set; }
        [Required]
        [MaxLength(30)]
        [Display(Name = "Apellido Materno")]
        public string apellidoMaterno { get; set; }
        [Required]
        [MaxLength(30)]
        [Display(Name = "Apellido Paterno")]
        public string apellidoPaterno { get; set; }
        [Required]
        [Display(Name = "Fecha de Nacimiento")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime fechaNacimiento { get; set; }
        [Required]
        [MaxLength(1)]
        [Display(Name = "Genero")]
        public string genero { get; set; }
        [Required]
        [MaxLength(30)]
        [Display(Name = "Fecha de Ingreso")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public string fechaIngreso { get; set; }
        [Required]       
        [Display(Name = "Activo")]
        public bool activo { get; set; }

        [Required]
        [MaxLength(25)]
        public string RFC { get; set; }




                                                          

                                                  
    }
}