﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoAlumno.Models
{
    interface IAlumnoBaseDatosRepository: IAlumnoRepository
    {
        bool GuardarInfoAlumnos(Alumno alumno);
        IEnumerable<Alumno> ListadoAlumnos { get; }
    }
}
