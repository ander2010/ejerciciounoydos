﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProyectoAlumno.Models
{
    public class AlumnoViewModel
    {
        public int id { get; set; }
      
        public string nombre { get; set; }
        
        public string apellidoMaterno { get; set; }
       
        public string apellidoPaterno { get; set; }
        

        public DateTime fechaNacimiento { get; set; }
       
        public string genero { get; set; }
        
        public string fechaIngreso { get; set; }
        
        public Boolean activo { get; set; }

       
        public string RFC { get; set; }
    }
}