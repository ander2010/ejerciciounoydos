﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoAlumno.Models
{
   public  interface IAlumnoFicheroRepository:IAlumnoRepository
    {
        bool GuardarInfoAlumnos(Alumno alumno, string ruta);
        IEnumerable<Alumno> ListadoAlumnos(string ruta);
    }
}
