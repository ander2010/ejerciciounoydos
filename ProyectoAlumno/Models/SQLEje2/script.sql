USE [master]
GO
/****** Object:  Database [RegistroAlumnos]    Script Date: 11/01/2020 17:36:05 ******/
CREATE DATABASE [RegistroAlumnos]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'RegistroAlumnos', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\RegistroAlumnos.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'RegistroAlumnos_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\RegistroAlumnos_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [RegistroAlumnos] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [RegistroAlumnos].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [RegistroAlumnos] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [RegistroAlumnos] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [RegistroAlumnos] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [RegistroAlumnos] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [RegistroAlumnos] SET ARITHABORT OFF 
GO
ALTER DATABASE [RegistroAlumnos] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [RegistroAlumnos] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [RegistroAlumnos] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [RegistroAlumnos] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [RegistroAlumnos] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [RegistroAlumnos] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [RegistroAlumnos] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [RegistroAlumnos] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [RegistroAlumnos] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [RegistroAlumnos] SET  DISABLE_BROKER 
GO
ALTER DATABASE [RegistroAlumnos] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [RegistroAlumnos] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [RegistroAlumnos] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [RegistroAlumnos] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [RegistroAlumnos] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [RegistroAlumnos] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [RegistroAlumnos] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [RegistroAlumnos] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [RegistroAlumnos] SET  MULTI_USER 
GO
ALTER DATABASE [RegistroAlumnos] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [RegistroAlumnos] SET DB_CHAINING OFF 
GO
ALTER DATABASE [RegistroAlumnos] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [RegistroAlumnos] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [RegistroAlumnos]
GO
/****** Object:  Table [dbo].[students]    Script Date: 11/01/2020 17:36:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[students](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](45) NOT NULL,
	[lastnameP] [varchar](45) NOT NULL,
	[lastnameM] [varchar](45) NULL,
	[birthdate] [date] NULL,
	[gender] [varchar](45) NULL,
	[admission_date] [datetime2](0) NULL,
	[active] [bit] NULL,
	[rfc] [varchar](45) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[subject]    Script Date: 11/01/2020 17:36:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[subject](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](45) NULL,
	[profesor] [varchar](45) NULL,
	[schedule] [time](0) NULL,
	[period] [varchar](45) NULL,
	[active] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[subject_student]    Script Date: 11/01/2020 17:36:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[subject_student](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[student_id] [int] NOT NULL,
	[subject_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[students] ON 

INSERT [dbo].[students] ([id], [name], [lastnameP], [lastnameM], [birthdate], [gender], [admission_date], [active], [rfc]) VALUES (1, N'Ander', N'Guzmán', N'S', CAST(N'2020-02-20' AS Date), N'M', CAST(N'2020-02-20T00:00:00.0000000' AS DateTime2), 1, N'474563')
INSERT [dbo].[students] ([id], [name], [lastnameP], [lastnameM], [birthdate], [gender], [admission_date], [active], [rfc]) VALUES (2, N'Rolo', N'sdds', N'S', CAST(N'2020-02-20' AS Date), N'M', CAST(N'2020-02-20T00:00:00.0000000' AS DateTime2), 1, N'474563')
INSERT [dbo].[students] ([id], [name], [lastnameP], [lastnameM], [birthdate], [gender], [admission_date], [active], [rfc]) VALUES (3, N'Folo', N'letal', N'S', CAST(N'2020-02-20' AS Date), N'M', CAST(N'2020-02-20T00:00:00.0000000' AS DateTime2), 1, N'474563')
INSERT [dbo].[students] ([id], [name], [lastnameP], [lastnameM], [birthdate], [gender], [admission_date], [active], [rfc]) VALUES (4, N'Filandal', N'guco', N'S', CAST(N'2020-02-20' AS Date), N'M', CAST(N'2020-02-20T00:00:00.0000000' AS DateTime2), 1, N'474563')
INSERT [dbo].[students] ([id], [name], [lastnameP], [lastnameM], [birthdate], [gender], [admission_date], [active], [rfc]) VALUES (5, N'Reinier', N'Gusmán', N'S', CAST(N'2020-02-20' AS Date), N'M', CAST(N'2020-02-20T00:00:00.0000000' AS DateTime2), 1, N'474563')
INSERT [dbo].[students] ([id], [name], [lastnameP], [lastnameM], [birthdate], [gender], [admission_date], [active], [rfc]) VALUES (6, N'Yadian', N'Santo', N'S', CAST(N'2020-02-20' AS Date), N'M', CAST(N'2020-02-20T00:00:00.0000000' AS DateTime2), 1, N'474563')
INSERT [dbo].[students] ([id], [name], [lastnameP], [lastnameM], [birthdate], [gender], [admission_date], [active], [rfc]) VALUES (7, N'Agustin', N'Mecanico', N'S', CAST(N'2020-02-20' AS Date), N'M', CAST(N'2020-02-20T00:00:00.0000000' AS DateTime2), 1, N'474563')
INSERT [dbo].[students] ([id], [name], [lastnameP], [lastnameM], [birthdate], [gender], [admission_date], [active], [rfc]) VALUES (8, N'Oscar', N'Lima', N'S', CAST(N'2020-02-20' AS Date), N'M', CAST(N'2020-02-20T00:00:00.0000000' AS DateTime2), 1, N'474563')
INSERT [dbo].[students] ([id], [name], [lastnameP], [lastnameM], [birthdate], [gender], [admission_date], [active], [rfc]) VALUES (9, N'Ander', N'Gozale', N'S', CAST(N'2020-02-20' AS Date), N'M', CAST(N'2020-02-20T00:00:00.0000000' AS DateTime2), 1, N'474563')
INSERT [dbo].[students] ([id], [name], [lastnameP], [lastnameM], [birthdate], [gender], [admission_date], [active], [rfc]) VALUES (10, N'Canto', N'Guzmán', N'S', CAST(N'2020-02-20' AS Date), N'M', CAST(N'2020-02-20T00:00:00.0000000' AS DateTime2), 1, N'474563')
INSERT [dbo].[students] ([id], [name], [lastnameP], [lastnameM], [birthdate], [gender], [admission_date], [active], [rfc]) VALUES (11, N'Bisery', N'Guzmán', N'S', CAST(N'2020-02-20' AS Date), N'M', CAST(N'2020-02-20T00:00:00.0000000' AS DateTime2), 1, N'474563')
INSERT [dbo].[students] ([id], [name], [lastnameP], [lastnameM], [birthdate], [gender], [admission_date], [active], [rfc]) VALUES (12, N'Cantinflas', N'Serct', N'S', CAST(N'2020-02-20' AS Date), N'M', CAST(N'2020-02-20T00:00:00.0000000' AS DateTime2), 1, N'474563')
INSERT [dbo].[students] ([id], [name], [lastnameP], [lastnameM], [birthdate], [gender], [admission_date], [active], [rfc]) VALUES (13, N'Bernabe', N'Gusmán', N'S', CAST(N'2020-02-20' AS Date), N'M', CAST(N'2020-02-20T00:00:00.0000000' AS DateTime2), 1, N'474563')
INSERT [dbo].[students] ([id], [name], [lastnameP], [lastnameM], [birthdate], [gender], [admission_date], [active], [rfc]) VALUES (14, N'Goliat', N'Gusmán', N'S', CAST(N'2020-02-20' AS Date), N'M', CAST(N'2020-02-20T00:00:00.0000000' AS DateTime2), 1, N'474563')
INSERT [dbo].[students] ([id], [name], [lastnameP], [lastnameM], [birthdate], [gender], [admission_date], [active], [rfc]) VALUES (15, N'David', N'Lazaro', N'S', CAST(N'2020-02-20' AS Date), N'M', CAST(N'2020-02-20T00:00:00.0000000' AS DateTime2), 1, N'474563')
INSERT [dbo].[students] ([id], [name], [lastnameP], [lastnameM], [birthdate], [gender], [admission_date], [active], [rfc]) VALUES (16, N'Lazaro', N'Gusmán', N'S', CAST(N'2020-02-20' AS Date), N'M', CAST(N'2020-02-20T00:00:00.0000000' AS DateTime2), 1, N'474563')
INSERT [dbo].[students] ([id], [name], [lastnameP], [lastnameM], [birthdate], [gender], [admission_date], [active], [rfc]) VALUES (17, N'Raegar', N'Gusmán', N'S', CAST(N'2020-02-20' AS Date), N'M', CAST(N'2020-02-20T00:00:00.0000000' AS DateTime2), 1, N'474563')
SET IDENTITY_INSERT [dbo].[students] OFF
SET IDENTITY_INSERT [dbo].[subject] ON 

INSERT [dbo].[subject] ([id], [title], [profesor], [schedule], [period], [active]) VALUES (1, N'Cálculo Diferencial', N'German', CAST(N'08:00:00' AS Time), N'mañana', 1)
INSERT [dbo].[subject] ([id], [title], [profesor], [schedule], [period], [active]) VALUES (2, N'Lolo', N'German', CAST(N'09:00:00' AS Time), N'tarde', 1)
INSERT [dbo].[subject] ([id], [title], [profesor], [schedule], [period], [active]) VALUES (3, N'Mnicure', N'German', CAST(N'10:00:00' AS Time), N'noche', 1)
INSERT [dbo].[subject] ([id], [title], [profesor], [schedule], [period], [active]) VALUES (4, N'Mino', N'German', CAST(N'08:00:00' AS Time), N'mañana', 1)
INSERT [dbo].[subject] ([id], [title], [profesor], [schedule], [period], [active]) VALUES (15, N'Calculo', N'German', CAST(N'08:00:00' AS Time), N'mañana', 1)
INSERT [dbo].[subject] ([id], [title], [profesor], [schedule], [period], [active]) VALUES (16, N'Fisica', N'German', CAST(N'08:00:00' AS Time), N'mañana', 1)
INSERT [dbo].[subject] ([id], [title], [profesor], [schedule], [period], [active]) VALUES (17, N'Matematica', N'German', CAST(N'08:00:00' AS Time), N'mañana', 1)
SET IDENTITY_INSERT [dbo].[subject] OFF
SET IDENTITY_INSERT [dbo].[subject_student] ON 

INSERT [dbo].[subject_student] ([id], [student_id], [subject_id]) VALUES (1, 1, 1)
INSERT [dbo].[subject_student] ([id], [student_id], [subject_id]) VALUES (2, 2, 1)
INSERT [dbo].[subject_student] ([id], [student_id], [subject_id]) VALUES (3, 3, 1)
INSERT [dbo].[subject_student] ([id], [student_id], [subject_id]) VALUES (4, 4, 1)
INSERT [dbo].[subject_student] ([id], [student_id], [subject_id]) VALUES (5, 5, 1)
INSERT [dbo].[subject_student] ([id], [student_id], [subject_id]) VALUES (6, 6, 1)
INSERT [dbo].[subject_student] ([id], [student_id], [subject_id]) VALUES (7, 7, 1)
INSERT [dbo].[subject_student] ([id], [student_id], [subject_id]) VALUES (8, 8, 1)
INSERT [dbo].[subject_student] ([id], [student_id], [subject_id]) VALUES (9, 9, 1)
INSERT [dbo].[subject_student] ([id], [student_id], [subject_id]) VALUES (10, 10, 1)
INSERT [dbo].[subject_student] ([id], [student_id], [subject_id]) VALUES (11, 11, 1)
INSERT [dbo].[subject_student] ([id], [student_id], [subject_id]) VALUES (12, 12, 1)
INSERT [dbo].[subject_student] ([id], [student_id], [subject_id]) VALUES (13, 13, 1)
INSERT [dbo].[subject_student] ([id], [student_id], [subject_id]) VALUES (14, 14, 1)
INSERT [dbo].[subject_student] ([id], [student_id], [subject_id]) VALUES (15, 15, 1)
INSERT [dbo].[subject_student] ([id], [student_id], [subject_id]) VALUES (16, 16, 1)
INSERT [dbo].[subject_student] ([id], [student_id], [subject_id]) VALUES (17, 17, 1)
INSERT [dbo].[subject_student] ([id], [student_id], [subject_id]) VALUES (18, 1, 2)
INSERT [dbo].[subject_student] ([id], [student_id], [subject_id]) VALUES (19, 2, 2)
INSERT [dbo].[subject_student] ([id], [student_id], [subject_id]) VALUES (20, 3, 2)
INSERT [dbo].[subject_student] ([id], [student_id], [subject_id]) VALUES (21, 4, 2)
INSERT [dbo].[subject_student] ([id], [student_id], [subject_id]) VALUES (22, 5, 2)
INSERT [dbo].[subject_student] ([id], [student_id], [subject_id]) VALUES (24, 6, 2)
INSERT [dbo].[subject_student] ([id], [student_id], [subject_id]) VALUES (25, 7, 2)
SET IDENTITY_INSERT [dbo].[subject_student] OFF
ALTER TABLE [dbo].[students] ADD  DEFAULT (NULL) FOR [lastnameM]
GO
ALTER TABLE [dbo].[students] ADD  DEFAULT (NULL) FOR [birthdate]
GO
ALTER TABLE [dbo].[students] ADD  DEFAULT (NULL) FOR [gender]
GO
ALTER TABLE [dbo].[students] ADD  DEFAULT (getdate()) FOR [admission_date]
GO
ALTER TABLE [dbo].[students] ADD  DEFAULT (NULL) FOR [active]
GO
ALTER TABLE [dbo].[students] ADD  DEFAULT (NULL) FOR [rfc]
GO
ALTER TABLE [dbo].[subject] ADD  DEFAULT (NULL) FOR [title]
GO
ALTER TABLE [dbo].[subject] ADD  DEFAULT (NULL) FOR [profesor]
GO
ALTER TABLE [dbo].[subject] ADD  DEFAULT (NULL) FOR [schedule]
GO
ALTER TABLE [dbo].[subject] ADD  DEFAULT (NULL) FOR [period]
GO
ALTER TABLE [dbo].[subject] ADD  DEFAULT (NULL) FOR [active]
GO
USE [master]
GO
ALTER DATABASE [RegistroAlumnos] SET  READ_WRITE 
GO
