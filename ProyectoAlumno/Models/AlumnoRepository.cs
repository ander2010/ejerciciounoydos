﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web;

namespace ProyectoAlumno.Models
{
    public class AlumnoRepository : IAlumnoFicheroRepository
    {
        public IEnumerable<Alumno> ListadoAlumnos(string ruta) {
            List<Alumno> ListadoAlumno = new List<Alumno>();
            string path = string.Empty;

            if (!Directory.Exists(ruta))
                    return null;

            string[] ficheros = Directory.GetFiles(ruta);
            for (int i = 0; i < ficheros.Length; i++)
            {

                path = ficheros[i];
                BinaryFormatter BF = new BinaryFormatter();
                FileStream Archivo = File.Open(path, FileMode.Open);
               Alumno DatosCargados = (Alumno)BF.Deserialize(Archivo);
                Archivo.Close();
                ListadoAlumno.Add(DatosCargados);
            }



            return ListadoAlumno;
        
        }

        public bool GuardarInfoAlumnos(Alumno alumno,string ruta)
        {
            bool respuestaAlumnoGuadado =false;
                   try
            {
                if (!(Directory.Exists(ruta)))
                {
                    Directory.CreateDirectory(ruta);
                }
                string path = ruta + alumno.RFC + ".txt";
                BinaryFormatter BF = new BinaryFormatter();
                FileStream Archivo = File.OpenWrite(path);
                BF.Serialize(Archivo, alumno);
                respuestaAlumnoGuadado = true;
                Archivo.Close();

            }
            catch (Exception e)
            {
                respuestaAlumnoGuadado = false;
                Debug.WriteLine("Error Salvando el fichero"+ e.Message);
            }
            return respuestaAlumnoGuadado;
            
        }

    }
}