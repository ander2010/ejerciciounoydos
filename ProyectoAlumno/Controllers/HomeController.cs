﻿using ProyectoAlumno.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectoAlumno.Controllers
{
    public class HomeController : Controller
    {
        public IAlumnoFicheroRepository alumnoRepositorio;

        public HomeController()
        {
            alumnoRepositorio = new AlumnoRepository();
        }
        public ActionResult Index()
        {
            string urlbase = Server.MapPath("~/Alumnos/");
            IEnumerable<Alumno> ListadoAlumno = alumnoRepositorio.ListadoAlumnos(urlbase);

            return View(ListadoAlumno);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        
        }

        [HttpPost]
        public ActionResult Create(Alumno alumno)
        {

      //      string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
      //Request.ApplicationPath.TrimEnd('/') + "\\";
            //string urlbase = baseUrl + "OficioSimple/PDFactiva/" + rC_Oficio_Simple.id;
            string urlbase = Server.MapPath("~/Alumnos/");

            if (ModelState.IsValid)
            {
               bool respuesta= alumnoRepositorio.GuardarInfoAlumnos(alumno, urlbase);
                if (respuesta)
                {
                    return RedirectToAction("Index");
                }
                else {

                    return View("Create");
                }          

              
            }
            return View("Create");

        }

    }
}